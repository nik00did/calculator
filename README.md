Класс Calculator
================
Описание
--------
Класс `Calculator`, написанный в файле `index.js`, состоит из четырех методов: `add()`, `substrаct()`, `multiply` и `divide`.
Каждый метод выполняет соответствующую математическую операцию с проверкой на входные данные. 
В случае неправельного ввода выбедет строку 'ERROR'.

Примеры:
--------
Метод `add()`:
```sh
add = (firstArgument, secondArgument) => {
    if ((!firstArgument && firstArgument !== 0) || (!secondArgument && secondArgument !== 0)) {
        return 'Error';
    }
        
    return firstArgument + secondArgument;
};
```
Метод `substract()`:
```sh
subtract = (a, b) => {
    if ((!a && a !== 0) || (!b && b !== 0)) {
        return 'Error';
    }

    return a - b;
};
```
Метод `multiply()`:
```sh
multiply = (firstArgument, secondArgument) => {
    if ((!firstArgument && firstArgument !== 0) || (!secondArgument && secondArgument !== 0)) {
        return 'Error';
    }

    return firstArgument * secondArgument;
};
```
Метод `divide()`:
```sh
divide = (a, b) => {
	if ((!a && a !==0) || !b) {
        return 'Error, enter correctly all arguments';
    }
		
    return a / b;
};
```

Тесты
-----
При написании тестов была использована библиотека Mocha и фреймворк Chai.
Тысты написаны к каждому из четырех методов и проверяют все возможные варианты принятия параметров входжения, тем самым показывая что метод работает так как нужно.
Примеры тестов:
Для премера преведен один метод `devide()` с тремя тестами, чтобы показать общую концепцию написания тестов. 
Метод `divide()`:
```sh
describe('Calculator divide', () => {
    const calculator = new Calculator();

    it(`should return an error if a = undefined, b = 5`, () => {
        const a = undefined;
        const b = 5;
        const expected = 'Error, enter correctly all arguments';
        
        const actual = calculator.divide(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return an error if a = 5, b = undefined`, () => {
        const a = 5;
        const b = undefined;
        const expected = 'Error, enter correctly all arguments';

        const actual = calculator.divide(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return error if a = null, b = 4`, () => {
        const a = null;
        const b = 4;
        const expected = 'Error, enter correctly all arguments';

        const actual = calculator.divide(a, b);

        assert.deepEqual(actual, expected);
    });
```