﻿﻿﻿describe('Calculator divide', () => {
    const calculator = new Calculator();

    it(`should return an error if a = undefined, b = 5`, () => {
        const a = undefined;
        const b = 5;
        const expected = 'Error, enter correctly all arguments';

        const actual = calculator.divide(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return an error if a = 5, b = undefined`, () => {
        const a = 5;
        const b = undefined;
        const expected = 'Error, enter correctly all arguments';
        const actual = calculator.divide(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return error if a = null, b = 4`, () => {
        const a = null;
        const b = 4;
        const expected = 'Error, enter correctly all arguments';

        const actual = calculator.divide(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return an error if a = 4, b = null`, () => {
        const a = 4;
        const b = null;
        const expected = 'Error, enter correctly all arguments';

        const actual = calculator.divide(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return an error if a = 15, b = 0`, () => {
        const a = 15;
        const b = 0;
        const expected = 'Error, enter correctly all arguments';

        const actual = calculator.divide(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return 3 if a = '15', b = 5`, () => {
        const a = '15';
        const b = 5;
        const expected = 3;

        const actual = calculator.divide(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return 5 if a = 25, b = '5'`, () => {
        const a = 25;
        const b = '5';
        const expected = 5;

        const actual = calculator.divide(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return 2 if a = 10, b = 5`, () => {
        const a = 10;
        const b = 5;
        const expected = 2;

        const actual = calculator.divide(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return 12.5 if a = 25, b = 2`, () => {
        const a = 25;
        const b = 2;
        const expected = 12.5;

        const actual = calculator.divide(a, b);

        assert.deepEqual(actual, expected);
    });
});

describe('Calculator subtract', () => {
    const calculator = new Calculator();

    it(`should return an error if a = undefined, b = 5`, () => {
        const a = undefined;
        const b = 5;
        const expected = 'Error';

        const actual = calculator.subtract(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return an error if a = 5, b = undefined`, () => {
        const a = 5;
        const b = undefined;
        const expected = 'Error';

        const actual = calculator.subtract(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return -4 if a = 0, b = 4`, () => {
        const a = 0;
        const b = 4;
        const expected = -4;

        const actual = calculator.subtract(a, b);

        assert.deepEqual(actual, expected);
    });

    it (`should return 4 if a = 4, b = 0`, () => {
        const a = 4;
        const b = 0;
        const expected = 4;

        const actual = calculator.subtract(a, b);

        assert.deepEqual(actual, expected);
    });

    it (`should return an error if a = 4, b = null`, () => {
        const a = 4;
        const b = 4;
        const expected = 0;

        const actual = calculator.subtract(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return 10 if a = '15', b = 5`, () => {
        const a = '15';
        const b = 5;
        const expected = 10;

        const actual = calculator.subtract(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return 20 if a = 25, b = '5'`, () => {
        const a = 25;
        const b = '5';
        const expected = 20;

        const actual = calculator.subtract(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return 5 if a = 10, b = 5`, () => {
        const a = 10;
        const b = 5;
        const expected = 5;

        const actual = calculator.subtract(a, b);

        assert.deepEqual(actual, expected);
    });
});

describe('Calculator add', () => {
    const calculator = new Calculator();

    it(`should return an error if a = undefined, b = 5`, () => {
        const a = undefined;
        const b = 5;
        const expected = 'Error';

        const actual = calculator.add(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return an error if a = 5, b = undefined`, () => {
        const a = 5;
        const b = undefined;
        const expected = 'Error';

        const actual = calculator.add(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return -4 if a = 0, b = 4`, () => {
        const a = 0;
        const b = 4;
        const expected = 4;

        const actual = calculator.add(a, b);

        assert.deepEqual(actual, expected);
    });

    it (`should return 4 if a = 4, b = 0`, () => {
        const a = 4;
        const b = 0;
        const expected = 4;

        const actual = calculator.add(a, b);

        assert.deepEqual(actual, expected);
    });

    it (`should return an error if a = 4, b = null`, () => {
        const a = 4;
        const b = 4;
        const expected = 8;

        const actual = calculator.add(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return 10 if a = 15, b = 5`, () => {
        const a = 15;
        const b = 5;
        const expected = 20;

        const actual = calculator.add(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return 20 if a = 25, b = 5`, () => {
        const a = 25;
        const b = 5;
        const expected = 30;

        const actual = calculator.add(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return 5 if a = 10, b = 5`, () => {
        const a = 10;
        const b = 5;
        const expected = 15;

        const actual = calculator.add(a, b);

        assert.deepEqual(actual, expected);
    });
});

describe('Calculator multiply', () => {
    const calculator = new Calculator();

    it(`should return an error if a = undefined, b = 5`, () => {
        const a = undefined;
        const b = 5;
        const expected = 'Error';

        const actual = calculator.multiply(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return an error if a = 5, b = undefined`, () => {
        const a = 5;
        const b = undefined;
        const expected = 'Error';

        const actual = calculator.multiply(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return error if a = null, b = 4`, () => {
        const a = null;
        const b = 4;
        const expected = 'Error';

        const actual = calculator.multiply(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return an error if a = 4, b = null`, () => {
        const a = 4;
        const b = null;
        const expected = 'Error';

        const actual = calculator.multiply(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return an error if a = 15, b = 0`, () => {
        const a = 15;
        const b = 0;
        const expected = 0;

        const actual = calculator.multiply(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return 3 if a = 15, b = 5`, () => {
        const a = 15;
        const b = 5;
        const expected = 75;

        const actual = calculator.multiply(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return 5 if a = 25, b = 5`, () => {
        const a = 25;
        const b = 5;
        const expected = 125;

        const actual = calculator.multiply(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return 2 if a = 10, b = 5`, () => {
        const a = 10;
        const b = 5;
        const expected = 50;

        const actual = calculator.multiply(a, b);

        assert.deepEqual(actual, expected);
    });

    it(`should return 12.5 if a = 25, b = 2`, () => {
        const a = 25;
        const b = 2;
        const expected = 50;

        const actual = calculator.multiply(a, b);
        
        assert.deepEqual(actual, expected);
    });
});